//
//  ViewController.swift
//  Autolayout
//
//  Created by Chính Trình Quang on 1/5/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 70)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        cell.layer.cornerRadius = 9
        cell.layer.borderWidth = 5
        cell.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        return cell
    }
    
    @IBOutlet weak var img: UIButton!
    
  
   
    @IBOutlet weak var collectionview: UICollectionView!
    var isShow = false
    @IBAction func btnFlip(_ sender: Any) {
        if isShow == false{
            isShow = true
            img.setImage(#imageLiteral(resourceName: "show"), for: .normal)
            UIView.transition(with: img, duration: 0.4, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        }else if isShow == true{
            isShow = false
            img.setImage(#imageLiteral(resourceName: "default"), for: .normal)
            UIView.transition(with: img, duration: 0.4, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        collectionview.dataSource = self
        collectionview.delegate = self
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let centerX = scrollView.contentOffset.x + scrollView.frame.size.width/2
        print(centerX)
        for cell in collectionview.visibleCells {
            
            var offsetX = centerX - cell.center.x
            if offsetX < 0 {
                offsetX *= -1
            }
            
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
            if offsetX > 50 {
                
                let offsetPercentage = (offsetX - 50) / view.bounds.width
                var scaleX = 1-offsetPercentage
                
                if scaleX < 0.8 {
                    scaleX = 0.8
                }
                cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
            }
        }
    }
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
}

